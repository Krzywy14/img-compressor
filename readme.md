# IMAGE COMPRESSOR 
## based on gulp

### Required instaled
- [GraphicsMagick](ftp://ftp.graphicsmagick.org/pub/GraphicsMagick/windows/GraphicsMagick-1.3.26-Q16-win64-dll.exe)

### #1 Geting start

By comand line make folder for compressor
```bash
	mkdir <dir-name> && cd <dir-name>
```

Clone repository
```bash
	git clone git clone git@bitbucket.org:adream_zen/imgmini.git _project
```

Make folder for images. Name must be "images"
```bash
	mkdir images
```
Instal npm modules
```bash
	npm install
```

For use compressor, type comand:
```bash
	gulp 
```

* * *

###### Include
- [node.js](https://nodejs.org/en/)
- [gulp.js](http://gulpjs.com) + tasks
- [ECMAScript 6](https://babeljs.io/learn-es2015/) [PL](http://mateuszroth.pl/komp/article-javascriptes6.php)
- Babel
- Image resize and minification

* * *