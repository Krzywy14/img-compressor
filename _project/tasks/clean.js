'use strict'

import "gulp";
import del			from "del"

import config		from './config';

export default (done) => {
	del.sync(config.clean_pattern, {force:true});
	done();
}
