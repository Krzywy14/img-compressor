'use strict';

import gulp 		from 'gulp';
import watch		from 'gulp-watch';
import config		from './config'

export default () => {
	gulp.watch( config.images.source, 		['images']);
}