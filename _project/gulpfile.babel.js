'use strict'

import gulp			from 'gulp';
// import config		from './tasks/config';
import './tasks/config';
// import runSequence 	from 'run-sequence';

// gulp tasks

import clean					from "./tasks/clean";
import images, {withoutResize}	from "./tasks/images";
import watch					from "./tasks/watch";

gulp.task('images',					images);
gulp.task('images:withoutResize',	withoutResize);
gulp.task('clean',					clean);
gulp.task('watch',					watch);

gulp.task('default', gulp.series('clean','images', () => done()));

gulp.task('run', gulp.series('default','watch', () => done()));

gulp.task('image', gulp.series('default', () => done()));

gulp.task('noresize', gulp.series('clean','images:withoutResize'));

gulp.task('noresize_run', gulp.series('clean','images:withoutResize','watch'));